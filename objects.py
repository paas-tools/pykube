import copy
import json
import time

import jsonpatch

import logging

from .exceptions import ObjectDoesNotExist
from .query import ObjectManager


DEFAULT_NAMESPACE = "default"


class APIObject(object):

    objects = ObjectManager()
    base = None
    namespace = None

    def __init__(self, api, obj):
        self.api = api
        self.set_obj(obj)

    def set_obj(self, obj):
        self.obj = obj
        self._original_obj = copy.deepcopy(obj)

    @property
    def name(self):
        return self.obj["metadata"]["name"]

    @property
    def creationTimestamp(self):
        return self.obj["metadata"]["creationTimestamp"]

    @property
    def uid(self):
        return self.obj["metadata"]["uid"]

    @property
    def annotations(self):
        if 'annotations' not in self.obj['metadata']:
            self.obj['metadata']['annotations'] = {}
        return self.obj["metadata"]["annotations"]

    @property
    def resourceVersion(self):
        return self.obj["metadata"].get("resourceVersion")

    @property
    def labels(self):
        if 'labels' not in self.obj['metadata']:
            self.obj['metadata']['labels'] = {}
        return self.obj["metadata"]["labels"]

    @property
    def finalizers(self):
        if 'finalizers' not in self.obj['metadata']:
            self.obj['metadata']['finalizers'] = []
        return self.obj["metadata"]["finalizers"]

    def api_kwargs(self, **kwargs):
        kw = {}
        collection = kwargs.pop("collection", False)
        if collection:
            kw["url"] = self.endpoint
        else:
            kw["url"] = "{}/{}".format(self.endpoint, self._original_obj["metadata"]["name"])
        if self.base:
            kw["base"] = self.base
        kw["version"] = self.version
        if self.namespace is not None:
            kw["namespace"] = self.namespace
        kw.update(kwargs)
        return kw

    def exists(self, ensure=False):
        r = self.api.get(**self.api_kwargs())
        if r.status_code not in {200, 404}:
            r.raise_for_status()
        if not r.ok:
            if ensure:
                raise ObjectDoesNotExist("{} does not exist.".format(self.name))
            else:
                return False
        return True

    def create(self):
        r = self.api.post(**self.api_kwargs(data=json.dumps(self.obj), collection=True))
        r.raise_for_status()
        self.set_obj(r.json())

    def reload(self):
        r = self.api.get(**self.api_kwargs())
        r.raise_for_status()
        self.set_obj(r.json())

    def update(self):
        patch = jsonpatch.make_patch(self._original_obj, self.obj)
        logging.debug("Applying patch: %s", patch)
        r = self.api.patch(**self.api_kwargs(
            headers={"Content-Type": "application/json-patch+json"},
            data=str(patch),
        ))
        r.raise_for_status()
        self.set_obj(r.json())

    def delete(self):
        r = self.api.delete(**self.api_kwargs())
        if r.status_code != 404:
            r.raise_for_status()

    @property
    def dirty(self):
        """Returns whether the object has been modified"""
        equivalent_values = [[], {}]
        for diff in jsonpatch.make_patch(self._original_obj, self.obj):
            if diff.get('op') == 'add' and diff.get('value') in equivalent_values:
                continue
            return True
        return False

class Namespace(APIObject):

    version = "v1"
    endpoint = "namespaces"

    @property
    def specFinalizers(self):
      return self.obj["spec"].get("finalizers", [])

    @property
    def phase(self):
      return self.obj["status"]["phase"]

    def finalize(self, finalizerName):
      """removes the finalizerName from the list of finalizers for that namespace"""
      tmp_obj = self._original_obj
      # remove finalizerName from list of finalizers
      tmp_obj["spec"]["finalizers"] = list(filter(lambda fn: fn != finalizerName, tmp_obj["spec"]["finalizers"]))
      kw = self.api_kwargs(data=json.dumps(tmp_obj))
      kw["url"] = kw["url"] + "/finalize"
      r = self.api.put(**kw)
      r.raise_for_status()
      self.set_obj(r.json())


class Node(APIObject):

    version = "v1"
    endpoint = "nodes"

class PersistentVolume(APIObject):

    version = "v1"
    endpoint = "persistentvolumes"
    base = "/api"

    @property
    def storage(self):
        return self.obj['spec']['capacity']['storage']

    @storage.setter
    def storage(self, value):
        self.obj['spec']['capacity']['storage'] = value

    @property
    def claimRef(self):
        return self.obj['spec'].get('claimRef')

    @claimRef.setter
    def claimRef(self, pvc):
        claimRef = {
            'kind': 'PersistentVolumeClaim',
            'name': pvc.name,
            'namespace': pvc.namespace,
            'uid': pvc.uid
        }
        self.obj['spec']['claimRef'] = claimRef

    @property
    def status(self):
        return self.obj['status']

    @status.setter
    def status(self, status):
        self.obj['status'] = status

    @property
    def phase(self):
        return self.obj['status'].get('phase')

    @property
    def storageClassName(self):
        return self.obj['spec'].get('storageClassName')

    @storageClassName.setter
    def storageClassName(self, storageClassName):
        self.obj['spec']['storageClassName'] = storageClassName

class StorageClass(APIObject):

    version = "storage.k8s.io/v1beta1"
    endpoint = "storageclasses"
    base = "/apis"

    @property
    def provisioner(self):
        return self.obj['provisioner']

    @property
    def parameters(self):
        return self.obj['parameters']

    @property
    def type(self):
        return self.obj['parameters']['type']

class Group(APIObject):

    version = "v1"
    endpoint = "groups"
    base = "/oapi"

    @property
    def users(self):
        return self.obj['users']

class NamespacedAPIObject(APIObject):

    objects = ObjectManager(namespace=DEFAULT_NAMESPACE)

    @property
    def namespace(self):
        if self.obj["metadata"].get("namespace"):
            return self.obj["metadata"]["namespace"]
        else:
            return DEFAULT_NAMESPACE


class Service(NamespacedAPIObject):

    version = "v1"
    endpoint = "services"


class Endpoint(NamespacedAPIObject):

    version = "v1"
    endpoint = "endpoints"


class Secret(NamespacedAPIObject):

    version = "v1"
    endpoint = "secrets"


class ReplicationController(NamespacedAPIObject):

    version = "v1"
    endpoint = "replicationcontrollers"

    @property
    def replicas(self):
        return self.obj["spec"]["replicas"]

    @replicas.setter
    def replicas(self, value):
        self.obj["spec"]["replicas"] = value

    def scale(self, replicas=None):
        if replicas is None:
            replicas = self.replicas
        self.exists(ensure=True)
        self.replicas = replicas
        self.update()
        while True:
            self.reload()
            if self.replicas == replicas:
                break
            time.sleep(1)


class Pod(NamespacedAPIObject):

    version = "v1"
    endpoint = "pods"

    @property
    def ready(self):
        cs = self.obj["status"]["conditions"]
        condition = next((c for c in cs if c["type"] == "Ready"), None)
        return condition is not None and condition["status"] == "True"


class DaemonSet(NamespacedAPIObject):

    version = "extensions/v1beta1"
    endpoint = "daemonsets"


class Route(NamespacedAPIObject):

    version = "v1"
    endpoint = "routes"
    base = "/oapi"

    @property
    def host(self):
        return self.obj["spec"].get("host")

    @host.setter
    def host(self, value):
        self.obj["spec"]["host"] = value

    @property
    def path(self):
        return self.obj["spec"].get("path")

    @path.setter
    def path(self, value):
        self.obj["spec"]["path"] = value

    @property
    def auto_generated(self):
        return self.annotations.get('openshift.io/host.generated') == 'true'

    # From Openshift 1.3 on, routes.spec.host is immutable. If we want to modify it,
    # we can call route.recreate and it will delete and re-create a new route with the updated
    # values (including route.spec.host)
    def recreate(self):
        self.delete()
        # OpenShift will reject creating a new object if these values are present
        del self.obj['status']
        del self.obj['metadata']['resourceVersion']
        del self.obj['metadata']['uid']
        del self.obj['metadata']['selfLink']
        self.create()

class Rolebinding(NamespacedAPIObject):

    version = "v1"
    endpoint = "rolebindings"
    base = "/oapi"

    @property
    def usernames(self):
        if 'userNames' not in self.obj:
            self.obj['userNames'] = []
        return self.obj['userNames']

    @usernames.setter
    def usernames(self, value):
        self.obj['userNames'] = value

    @property
    def subjects(self):
        if 'subjects' not in self.obj:
            self.obj['subjects'] = {}
        return self.obj['subjects']

    @subjects.setter
    def subjects(self, value):
        self.obj['subjects'] = value

    @property
    def role(self):
        return self.obj['roleRef']['name']

    @role.setter
    def role(self, value):
        self.obj['roleRef']['name'] = value

    @property
    def groups(self):
        return [subject['name']
                for subject in self.subjects
                if subject['kind'] == 'Group']

class Policybinding(NamespacedAPIObject):

    version = "v1"
    endpoint = "policybindings"
    base = "/oapi"

    @property
    def rolebindings(self):
        if 'roleBindings' not in self.obj:
            self.obj['roleBindings'] = []
        return self.obj['roleBindings']

    @property
    def groupNames(self):
        if 'roleBindings' not in self.obj:
            self.obj['roleBindings'] = []
        return self.obj['roleBindings']

class Serviceaccount(NamespacedAPIObject):

    version = "v1"
    endpoint = "serviceaccounts"
    base = "/api"

    @property
    def secrets(self):
        if 'secrets' not in self.obj:
            self.obj['secrets'] =  []
        return self.obj['secrets']

    def add_secret(self, secret_name):
        self.secrets.append({'name': secret_name })

class Resourcequota(NamespacedAPIObject):

    version = "v1"
    endpoint = "resourcequotas"
    base = "/api"

    @property
    def hard(self):
        if 'hard' not in self.obj['spec']:
            self.obj['spec']['hard'] =  {}
        return self.obj['spec']['hard']

    @hard.setter
    def hard(self, value):
        self.obj['spec']['hard'] = value


class PersistentVolumeClaim(NamespacedAPIObject):

    version = "v1"
    endpoint = "persistentvolumeclaims"
    base = "/api"

    @property
    def volumeName(self):
        return self.obj['spec'].get('volumeName')

    @volumeName.setter
    def volumeName(self, value):
        self.obj['spec']['volumeName'] = value

    @property
    def storage(self):
        return self.obj['spec']['resources']['requests'].get('storage')

    @property
    def phase(self):
        return self.obj['status'].get('phase')

    @property
    def accessModes(self):
        return self.obj['spec'].get('accessModes')

    @property
    def labelSelector(self):
        if 'selector' in self.obj['spec']:
            return self.obj['spec']['selector']['matchLabels']
        return None

    @property
    def storageClassName(self):
        return self.obj['spec'].get('storageClassName')
