import json
import unittest

from pykube.objects import *

class APIObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_route = """
        {
    "kind": "Route",
    "apiVersion": "v1",
    "metadata": {
        "name": "openshift-app-manager",
        "namespace": "openshift-app-manager",
        "finalizers": [
            "cern.ch/webservices-finalizer"
        ],
        "labels": {
            "app": "openshift-app-manager",
            "template": "openshift-app-manager"
        }
    },
    "spec": {},
    "status": {}
    }

        """
        self.route = Route(api=None, obj=json.loads(example_route))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.route.dirty)

    def test_dirty_check_field_not_present(self):
        # annotations are not present in the original routeect
        annotations = self.route.annotations
        self.assertEquals(self.route.annotations, {})
        self.assertFalse(self.route.dirty)

    def test_dirty_check_field_existing_already(self):
        # labels are not empty
        self.route.labels
        self.route.annotations
        self.assertFalse(self.route.dirty)

    def test_dirty_big_change(self):
        # annotations are not present in the original routeect
        self.route.annotations['random'] = 'example'
        self.assertEquals(self.route.annotations, {'random': 'example'})
        self.assertTrue(self.route.dirty)

    def test_dirty_change_existing_label(self):
        self.assertEquals(self.route.labels['app'], 'openshift-app-manager')
        self.route.labels['app'] = 'random'
        self.assertEquals(self.route.labels['app'], 'random')
        self.assertTrue(self.route.dirty)

    def test_dirty_remove_labels(self):
        self.assertEquals(self.route.labels['app'], 'openshift-app-manager')
        self.route.labels.clear()
        self.assertEquals(self.route.labels, {})
        self.assertTrue(self.route.dirty)

    def test_finalizers(self):
        self.assertEquals(self.route.finalizers, ['cern.ch/webservices-finalizer'])
        self.assertFalse(self.route.dirty)
        self.route.finalizers[0] = 'test-finalizer'
        self.assertEquals(self.route.finalizers, ['test-finalizer'])
        self.assertTrue(self.route.dirty)

class ServiceAccountObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_sa = """
{
    "kind": "ServiceAccount",
    "apiVersion": "v1",
    "metadata": {
        "name": "builder",
        "namespace": "openshift-app-manager",
        "creationTimestamp": "2016-04-04T08:53:22Z"
    },
    "secrets": [
        {
            "name": "builder-token"
        },
        {
            "name": "builder-dockercfg"
        },
        {
            "name": "sshdeploykey"
        }
    ],
    "imagePullSecrets": []
}

        """
        self.sa = Serviceaccount(api=None, obj=json.loads(example_sa))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.sa.dirty)

    def test_dirty_initialize(self):
        del self.sa.obj['secrets']
        self.assertTrue(self.sa.dirty)
        self.assertTrue('secrets' not in self.sa.obj)
        self.assertEquals(self.sa.secrets, [])

    def test_creation_timestamp(self):
        self.assertEquals(self.sa.creationTimestamp, "2016-04-04T08:53:22Z")
        self.assertFalse(self.sa.dirty)

    def test_read_secrets(self):
        secrets = self.sa.obj['secrets']
        self.assertEquals(self.sa.secrets, secrets)
        self.assertFalse(self.sa.dirty)

    def test_remove_secrets(self):
        # Empty list
        del self.sa.obj['secrets'][:]
        self.assertEquals(self.sa.secrets, [])
        self.assertTrue(self.sa.dirty)

    def test_add_secrets(self):
        secret_name = 'example'
        n_secrets = len(self.sa.secrets)
        self.assertFalse(self.sa.dirty)
        self.sa.add_secret(secret_name)
        self.assertTrue(self.sa.dirty)
        self.assertEquals(n_secrets + 1, len(self.sa.secrets))
        self.assertEquals({'name':'example'}, self.sa.secrets[-1])

class RoleBindingObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_rb = """
{
    "kind": "RoleBinding",
    "apiVersion": "v1",
    "metadata": {
        "name": "admin",
        "namespace": "project"
    },
    "userNames": [
        "rodrigal"
    ],
    "subjects": [
        {
            "kind": "User",
            "name": "rodrigal"
        },
        {
            "kind": "Group",
            "name": "group1"
        },
        {
            "kind": "Group",
            "name": "group2"
        }
    ],
    "roleRef": {
        "name": "basic-user"
    }
}
        """
        self.rb = Rolebinding(api=None, obj=json.loads(example_rb))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.rb.dirty)

    def test_dirty_initialize(self):
        del self.rb.obj['subjects']
        del self.rb.obj['userNames']
        self.assertTrue(self.rb.dirty)
        self.assertTrue('subjects' not in self.rb.obj)
        self.assertTrue('userNames' not in self.rb.obj)
        self.assertEquals(self.rb.subjects, {})
        self.assertEquals(self.rb.usernames, [])

    def test_read_subjects(self):
        subjects = self.rb.obj['subjects']
        self.assertEquals(self.rb.subjects, subjects)
        self.assertFalse(self.rb.dirty)

    def test_read_role(self):
        role = self.rb.obj['roleRef']['name']
        self.assertEquals(self.rb.role, role)
        self.assertFalse(self.rb.dirty)

    def test_change_role(self):
        self.assertEquals(self.rb.role, 'basic-user')
        self.rb.role = 'admin'
        self.assertEquals(self.rb.role, 'admin')
        self.assertTrue(self.rb.dirty)

    def test_read_groups(self):
        self.assertEquals(self.rb.groups, ['group1', 'group2'])
        self.assertFalse(self.rb.dirty)

class ResourceQuotaObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_rq = """
{
    "kind": "ResourceQuota",
    "apiVersion": "v1",
    "metadata": {
        "name": "example-quota",
        "namespace": "project"
    },
    "spec": {
        "hard": {
            "cpu": "2",
            "memory": "4Gi",
            "pods": "8",
            "resourcequotas": "1",
            "services": "8"
        }
    },
    "status": {
        "hard": {
            "cpu": "2",
            "memory": "4Gi",
            "pods": "8",
            "resourcequotas": "1",
            "services": "8"
        },
        "used": {
            "cpu": "750m",
            "memory": "1536Mi",
            "pods": "2",
            "resourcequotas": "1",
            "services": "2"
        }
    }
}
        """
        self.rq = Resourcequota(api=None, obj=json.loads(example_rq))
        self.flavors =  {
           'small':{
               "flavor_cpu": "1",
               "flavor_memory": "2Gi", # respect 2GB/CPU ratio in Openstack VMs
               "flavor_pods": "8",     # assuming default quota request/pod 256MB
               "flavor_services": "8"
        },
           'medium':{
               "flavor_cpu": "2",
               "flavor_memory": "4Gi",
               "flavor_pods": "16",
               "flavor_services": "16"
        },
           'large':{
               "flavor_cpu": "4",
               "flavor_memory": "8Gi",
               "flavor_pods": "32",
               "flavor_services": "32"
        }
       }

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.rq.dirty)

    def test_dirty_initialize(self):
        del self.rq.obj['spec']['hard']
        self.assertTrue(self.rq.dirty)
        self.assertTrue('hard' not in self.rq.obj['spec'])
        self.assertEquals(self.rq.hard, {})

    def test_read_quota(self):
        quota = self.rq.obj['spec']['hard']
        self.assertEquals(self.rq.hard, quota)
        self.assertFalse(self.rq.dirty)

    def test_set_quota(self):
        quota = self.flavors['medium']
        self.rq.hard = quota
        self.assertEquals(self.rq.hard, {
                "flavor_cpu": "2",
                "flavor_memory": "4Gi",
                "flavor_pods": "16",
                "flavor_services": "16"
         })
        self.assertTrue(self.rq.dirty)


class PersistentVolumeClaimObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_pvc = """
{
    "kind": "PersistentVolumeClaim",
    "apiVersion": "v1",
    "metadata": {
        "name": "myclaim",
        "namespace": "test-pvc"
    },
    "spec": {
        "accessModes": [
            "ReadWriteOnce"
        ],
        "selector": {
                    "matchLabels": {
                        "cvmfs-repository": "sft.cern.ch"
                    }
                },
        "resources": {
            "requests": {
                "storage": "5Gi"
            }
        },
        "volumeName": "pv0001",
        "storageClassName": "cvmfs"
    },
    "status": {
        "phase": "Bound",
        "accessModes": [
            "ReadWriteOnce"
        ],
        "capacity": {
            "storage": "5Gi"
        }
    }
}
        """
        self.pvc = PersistentVolumeClaim(api=None, obj=json.loads(example_pvc))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.pvc.dirty)

    def test_get_volumeName(self):
        self.assertEquals(self.pvc.volumeName, 'pv0001')
        self.assertFalse(self.pvc.dirty)

    def test_set_volumeName(self):
        self.pvc.volumeName = 'pv0002'
        self.assertEquals(self.pvc.volumeName, 'pv0002')
        self.assertTrue(self.pvc.dirty)

    def test_get_storage(self):
        self.assertEquals(self.pvc.storage, '5Gi')
        self.assertFalse(self.pvc.dirty)

    def test_get_phase(self):
        self.assertEquals(self.pvc.phase, 'Bound')
        self.assertFalse(self.pvc.dirty)

    def test_get_accessModes(self):
        self.assertEquals(self.pvc.accessModes, ["ReadWriteOnce"])
        self.assertFalse(self.pvc.dirty)

    def test_get_labelSelector(self):
        self.assertEqual(self.pvc.labelSelector, {'cvmfs-repository': "sft.cern.ch"})
        self.assertFalse(self.pvc.dirty)

    def test_get_storageClassName(self):
        self.assertEqual(self.pvc.storageClassName, 'cvmfs')
        self.assertFalse(self.pvc.dirty)

class PersistentVolumeObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_pv = """
{
    "kind": "PersistentVolume",
    "apiVersion": "v1",
    "metadata": {
        "name": "pv0001",
        "labels": {
            "storage-type": "nfs"
        }
    },
    "spec": {
        "capacity": {
            "storage": "5Gi"
        },
        "nfs": {
            "server": "example-nfs01.cern.ch",
            "path": "/mnt/example/001"
        },
        "accessModes": [
            "ReadWriteOnce"
        ],
        "claimRef": {
            "kind": "PersistentVolumeClaim",
            "namespace": "test-pvc-nfs",
            "name": "examplepvc",
            "uid": "0000-0000-0000-0000"
        },
        "persistentVolumeReclaimPolicy": "Retain",
        "storageClassName": "standard"
    },
    "status": {
        "phase": "Bound"
    }
}

        """
        self.pv = PersistentVolume(api=None, obj=json.loads(example_pv))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.pv.dirty)

    def test_get_storage(self):
        self.assertEquals(self.pv.storage, '5Gi')
        self.assertFalse(self.pv.dirty)

    def test_set_storage(self):
        self.pv.storage = '6Gi'
        self.assertEquals(self.pv.storage, '6Gi')
        self.assertTrue(self.pv.dirty)

    def test_get_claimRef(self):
        claimRef = {
            "kind": "PersistentVolumeClaim",
            "namespace": "test-pvc-nfs",
            "name": "examplepvc",
            "uid": "0000-0000-0000-0000"
        }
        self.assertEquals(self.pv.claimRef, claimRef)
        self.assertFalse(self.pv.dirty)

    def test_set_claimRef(self):
        example_pvc = """
{
    "kind": "PersistentVolumeClaim",
    "apiVersion": "v1",
    "metadata": {
        "name": "myclaim",
        "namespace": "test-pvc",
        "uid": "0000-0000-0000-0001"
    },
    "spec": {
        "accessModes": [
            "ReadWriteOnce"
        ],
        "resources": {
            "requests": {
                "storage": "5Gi"
            }
        },
        "volumeName": "pv0001"
    },
    "status": {
        "phase": "Bound",
        "accessModes": [
            "ReadWriteOnce"
        ],
        "capacity": {
            "storage": "5Gi"
        }
    }
}
        """
        pvc = PersistentVolumeClaim(api=None, obj=json.loads(example_pvc))
        expected_claimRef = {
            "kind": "PersistentVolumeClaim",
            "namespace": "test-pvc",
            "name": "myclaim",
            "uid": "0000-0000-0000-0001"
        }
        self.pv.claimRef = pvc
        self.assertEquals(self.pv.claimRef, expected_claimRef)
        self.assertTrue(self.pv.dirty)

    def test_get_status(self):
        self.assertEquals(self.pv.status, {'phase': 'Bound'})
        self.assertFalse(self.pv.dirty)

    def test_set_status(self):
        status = {
            'phase': 'Released',
            'message': 'Example message'
            }
        self.pv.status = status
        self.assertEquals(self.pv.status, status)
        self.assertTrue(self.pv.dirty)

    def test_get_phase(self):
        self.assertEquals(self.pv.phase, 'Bound')
        self.assertFalse(self.pv.dirty)

    def test_get_storageClassName(self):
        self.assertEqual(self.pv.storageClassName, 'standard')
        self.assertFalse(self.pv.dirty)

    def test_set_storageClassName(self):
        self.pv.storageClassName = 'other'
        self.assertEquals(self.pv.storageClassName, 'other')
        self.assertTrue(self.pv.dirty)

class RouteObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_route = """
{
    "kind": "Route",
    "apiVersion": "v1",
    "metadata": {
        "name": "jenkins-cli",
        "namespace": "test-route",
        "annotations": {
            "router.cern.ch/network-visibility": "Intranet"
        },
        "labels": {}
    },
    "spec": {
        "host": "test-route.web.cern.ch",
        "path": "/cli",
        "to": {
            "kind": "Service",
            "name": "test"
        },
        "port": {
            "targetPort": 8080
        },
        "tls": {
            "termination": "edge",
            "insecureEdgeTerminationPolicy": "Redirect"
        }
    },
    "status": {}
}

        """
        self.route = Route(api=None, obj=json.loads(example_route))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.route.dirty)

    def test_autogenerated_false(self):
        self.assertFalse(self.route.auto_generated)

    def test_autogenerated_true(self):
        self.route.annotations['openshift.io/host.generated'] = 'true'
        self.assertTrue(self.route.auto_generated)

    def test_get_host(self):
        self.assertEquals(self.route.host, 'test-route.web.cern.ch')

    def test_set_host(self):
        self.route.host = 'other-route.web.cern.ch'
        self.assertEquals(self.route.host, 'other-route.web.cern.ch')

    def test_get_path(self):
        self.assertEquals(self.route.path, '/cli')

class StorageClassObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_sc = """
{
	"kind": "StorageClass",
	"apiVersion": "storage.k8s.io/v1beta1",
	"metadata": {
		"name": "slow"
	},
	"provisioner": "kubernetes.io/aws-ebs",
	"parameters": {
		"type": "io1",
		"zone": "us-east-1d",
		"iopsPerGB": "10"
	}
}

        """
        self.sc = StorageClass(api=None, obj=json.loads(example_sc))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.sc.dirty)

    def test_get_provisioner(self):
        self.assertEquals(self.sc.provisioner, 'kubernetes.io/aws-ebs')

    def test_get_parameters(self):
        params = {
            'type': 'io1',
            'zone': 'us-east-1d',
            'iopsPerGB': '10'
        }
        self.assertEquals(self.sc.parameters, params)

class GroupsObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_group = """
{
    "apiVersion": "v1",
    "kind": "Group",
    "metadata": {
        "name": "group"
    },
    "users": [
        "user1",
        "user2"
    ]
}

        """
        self.group = Group(api=None, obj=json.loads(example_group))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.group.dirty)

    def test_get_users(self):
        self.assertEquals(self.group.users, ['user1', 'user2'])


class PolicybindingObjectTestCase(unittest.TestCase):

    def setUp(self):
        example_pb = """
{
    "apiVersion": "v1",
    "kind": "PolicyBinding",
    "metadata": {
        "name": ":default",
        "namespace": "paas-infra"
    },
    "policyRef": {
        "name": "default"
    },
    "roleBindings": [
        {
            "name": "admin",
            "roleBinding": {
                "groupNames": null,
                "metadata": {
                    "name": "admin",
                    "namespace": "paas-infra"
                },
                "roleRef": {
                    "name": "admin"
                },
                "subjects": [
                    {
                        "kind": "User",
                        "name": "user1"
                    }
                ],
                "userNames": [
                    "user1"
                ]
            }
        }
]}

        """
        self.pb = Policybinding(api=None, obj=json.loads(example_pb))

    # Test dirty
    def test_dirty_no_changes(self):
        self.assertFalse(self.pb.dirty)

    def test_get_rolebindings(self):
        other = """
[
    {
        "name": "admin",
        "roleBinding": {
            "groupNames": null,
            "metadata": {
                "name": "admin",
                "namespace": "paas-infra"
            },
            "roleRef": {
                "name": "admin"
            },
            "subjects": [
                {
                    "kind": "User",
                    "name": "user1"
                }
            ],
            "userNames": [
                "user1"
            ]
        }
    }
]
"""
        self.assertEquals(self.pb.rolebindings, json.loads(other))
